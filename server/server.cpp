﻿#include "win_server.h"

#include <stdio.h>

connection_info connections[MAX_CONNECTIONS];
bind_info binds[MAX_BINDS];
int hosts[MAX_HOSTS];

int main() {



	SOCKET listen_socket;
	SOCKADDR_IN sin;
	int initialize_status = initialize_server(&listen_socket, &sin);






	int connection_id = 0;
	int host_id = 0;
	int bind_id = 0;

	SOCKET new_connection;
	int size = sizeof(sin);
	for (int i = 0; i < MAX_CONNECTIONS; i++) {
		new_connection = accept(listen_socket, (SOCKADDR*)&sin, &size);
		if (new_connection == 0) {
			printf("error \n");
			continue;
		}

		connections[connection_id].connection_socket = new_connection;
		connections[connection_id].connection_id = connection_id;
		printf("connection id %d\n", connection_id);

		int is_connection_host = get_command_id(&new_connection);
		printf("is_connection_host %d\n", is_connection_host);
		if (is_connection_host == 1) {
			hosts[host_id] = connection_id;
			host_id++;
			send_number(&new_connection, connection_id);
		}
		else {
			int host = get_command_id(&new_connection);

			int in_hosts = 0;
			SOCKET host_socket;
			for (int j = 0; j < MAX_HOSTS; j++) {
				if (hosts[j] == host) {
					in_hosts = 1;
					break;
				}
			}
			if (in_hosts == 0) {
				send_number(&new_connection, 0);
			}
			else {
				for (int j = 0; j < MAX_CONNECTIONS; j++) {
					if (connections[j].connection_id == host) {
						host_socket = connections[j].connection_socket;
					}
				}
				binds[bind_id].client = new_connection;
				binds[bind_id].host = host_socket;

				HANDLE hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)client_host, (LPVOID*)&binds[bind_id], 0, NULL);
				bind_id++;
				send_number(&new_connection, connection_id);

				
				while (true) {
					int command = get_command_id(&new_connection);
					if (command == 0) {
						break;
					}
					send_number(&host_socket, command);
				}
			}
			connection_id++;
		}
	}





	return 0;
}
