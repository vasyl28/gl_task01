#pragma once
#include <winsock2.h>

#define MAX_CONNECTIONS 50
#define MAX_HOSTS 10
#define MAX_BINDS 10

struct connection_info {
	SOCKET connection_socket;
	int connection_id;
};

struct bind_info {
	SOCKET client;
	SOCKET host;
};

void get_process_name(SOCKET *client, char *process_name);
void send_process_name(SOCKET *host,char *process_name);

int initialize_server(SOCKET *listen_socket, SOCKADDR_IN *sin);

void send_number(SOCKET *socket, int num);
void send_message(SOCKET *socket, char *message);
int get_command_id(SOCKET *socket);

DWORD WINAPI client_host(LPVOID param);

union int_char {
	int number;
	char bytes[4];
};
