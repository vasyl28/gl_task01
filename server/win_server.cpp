#include "win_server.h"

#pragma comment(lib, "ws2_32.lib")
#include <winsock2.h>
#include <stdio.h>

#pragma warning(disable: 4996)


#define PORT 12321
#define IP "192.168.1.3"


int initialize_server(SOCKET *listen_socket, SOCKADDR_IN *sin) {
	WSAData wsa_data;
	WORD dll_version = MAKEWORD(2, 1);
	if (WSAStartup(dll_version, &wsa_data) != 0) {
		printf("startup error \n");
		return 1;
	}


	(*sin).sin_family = AF_INET;
	(*sin).sin_addr.s_addr = inet_addr(IP);
	(*sin).sin_port = htons(PORT);

	*listen_socket = socket(AF_INET, SOCK_STREAM, NULL);
	bind(*listen_socket, (SOCKADDR*)&(*sin), sizeof((*sin)));
	listen(*listen_socket, SOMAXCONN);

	return 0;
}


int get_command_id(SOCKET *socket) {
	int_char temp;
	recv(*socket, temp.bytes, sizeof(temp), NULL);
	return temp.number;
}


void send_number(SOCKET *socket, int num) {
	int_char temp;
	temp.number = num;
	send(*socket, temp.bytes, sizeof(num), NULL);
	return;
}

void get_process_name(SOCKET *client, char *process_name) {
	recv(*client, process_name, sizeof(*process_name), NULL);
	return;
}

void send_process_name(SOCKET *host, char *process_name) {
	send(*host, process_name, sizeof(*process_name), NULL);
	return;
}

void send_message(SOCKET *host, char *process_name) {
	send(*host, process_name, sizeof(*process_name), NULL);
	return;
}

DWORD WINAPI client_host(LPVOID param) {
	int command_id;
	char process_name[25];
	bind_info *bind = (bind_info*)&param;
	SOCKET client = bind->client;
	SOCKET host = bind->host;
	while (true) {
		command_id = get_command_id(&client);
		if (command_id == 0) {
			break;
		}
		else {
			get_process_name(&client, process_name);
			send_process_name(&host, process_name);
			send_number(&host, command_id);
			command_id = get_command_id(&host);
			char message1[30] = "process successfully running";
			char message2[30] = "process is already running";
			char message3[30] = "process is'nt running";
			char message4[30] = "process successfully closed";
			char message5[30] = "process is'nt running";
			char message6[30] = "process is running";
			char message7[30] = "unknown error";
			switch (command_id) {
			case 10:
				send_message(&client, message1);
				break;
			case 11:
				send_message(&client, message2);
				break;
			case 20:
				send_message(&client, message3);
				break;
			case 21:
				send_message(&client, message4);
				break;
			case 30:
				send_message(&client, message5);
				break;
			case 31:
				send_message(&client, message6);
				break;
			case 66:
				send_message(&client, message7);
				break;
			}
		}
	}

	ExitThread(0);
}





