#pragma once
#include <winsock2.h>

SOCKET connect_to_server(void);

void UserInterface(SOCKET *socket);
void client_functionality(SOCKET *socket);
void host_functionality(SOCKET *socket);

int run_process(char *process_name);
int is_process_run(char *process_name);
int close_process(char *process_name);

void get_process_name(SOCKET *client, char *process_name);
void send_process_name(SOCKET *host, char *process_name);
void get_message(SOCKET *socket, char *message);
void send_number(SOCKET *socket, int num);
int get_command_id(SOCKET *socket);

union int_char {
	int number;
	char bytes[4];
};
