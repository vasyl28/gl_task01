#include "win_client.h"



#pragma warning(disable: 4996)
#pragma comment(lib, "ws2_32.lib")
#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <windows.h>
#include <comdef.h>
#include "tlhelp32.h"

#define PORT 12321
#define IP "192.168.1.3"



SOCKET connect_to_server(void) {
	WSAData wsa_data;
	WORD dll_version = MAKEWORD(2, 1);
	if (WSAStartup(dll_version, &wsa_data) != 0) {
		std::cout << "WSAStartup doesn't work OK";
		exit(1);
	}

	SOCKADDR_IN sin;
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = inet_addr(IP);
	sin.sin_port = htons(PORT);

	SOCKET connection_socket = socket(AF_INET, SOCK_STREAM, NULL);
	if (connect(connection_socket, (SOCKADDR*)&sin, sizeof(sin)) != 0) {
		std::cout << "connection error";
		exit(1);
	}

	return connection_socket;

}

void send_number(SOCKET *socket, int num) {
	int_char temp;
	temp.number = num;
	send(*socket, temp.bytes, sizeof(num), NULL);
	return;
}

int get_command_id(SOCKET *socket) {
	int_char temp;
	recv(*socket, temp.bytes, sizeof(temp), NULL);
	return temp.number;
}

void get_process_name(SOCKET *client, char *process_name) {
	recv(*client, process_name, sizeof(*process_name), NULL);
	return;
}

void send_process_name(SOCKET *host, char *process_name) {
	send(*host, process_name, sizeof(*process_name), NULL);
	return;
}

void get_message(SOCKET *socket, char *message) {
	recv(*socket, message, sizeof(*message), NULL);
	return;
}

void host_functionality(SOCKET *socket) {
	int command_id;
	char process_name[25];
	int status;

	while (true) {
		get_process_name(socket, process_name);
		command_id = get_command_id(socket);
		switch (command_id) {
		case 1:
			status = is_process_run(process_name);
			if (status == 0) {
				send_number(socket, 11);
				break;
			}
			else {
				status = run_process(process_name);
				if (status == 0) {
					send_number(socket, 10);
					break;
				}
				send_number(socket, 11);
				break;
			}
		case 2:
			status = is_process_run(process_name);
			if (status == 1) {
				send_number(socket, 21);
				break;
			}
			else {
				status = close_process(process_name);
				if (status == 0) {
					send_number(socket, 20);
					break;
				}
				send_number(socket, 21);
				break;
			}
		case 3:
			status = is_process_run(process_name);
			if (status == 1) {
				send_number(socket, 31);
				break;
			}
			else {
				send_number(socket, 30);
				break;
			}
		default:
			send_number(socket, 66);
			break;
		}
	}
}

void client_functionality(SOCKET *socket) {
	int action;
	char process_name[25];
	char message[30];
	while (true) {
		printf("Choose an action: \n");
		printf("  0 - exit from the program \n");
		printf("  1 - run program \n");
		printf("  2 - close proram \n");
		printf("  3 - check program status \n");
		scanf("%d", &action);
		send_number(socket, action);
		gets_s(process_name);
		send_process_name(socket, process_name);
		get_message(socket, message);
		printf("%s \n", message);
	}
}

void UserInterface(SOCKET *socket) {
	int command_id;
	printf("Enter the number 1 if it is a remote host and number 2 to connect to the remote host: \n");

	scanf("%d", &command_id);

	switch (command_id) {
	case 1:
		printf("ok");
		send_number(socket, command_id);
		int client_id;
		client_id = get_command_id(socket);
		printf("id for connection: %d", client_id);
		host_functionality(socket);
		break;
	case 2:
		int host_id;
		send_number(socket, command_id);
		printf("Enter hostID: ");
		scanf("%d", &host_id);
		send_number(socket, host_id);
		int is_host_exist; 
		is_host_exist = get_command_id(socket);
		if (is_host_exist == 0) {
			printf("Invalid id\n");
		}
		else {
			client_functionality(socket);
		}
		break;
	default:
		printf("invalid input\n");
		break;
	}
}

int run_process(char *process_name) {
	wchar_t w_process_name[25];
	mbstowcs(w_process_name, process_name, strlen(process_name));
	LPCWSTR l_process_name = w_process_name;

	STARTUPINFO cif;
	ZeroMemory(&cif, sizeof(STARTUPINFO));
	PROCESS_INFORMATION pi;
	if (CreateProcess(l_process_name, NULL,
		NULL, NULL, FALSE, NULL, NULL, NULL, &cif, &pi) == 0) {
		return 1;
	}
	else {
		return 0;
	}
}

int is_process_run(char *process_name) {
	HANDLE hSnap = NULL;
	PROCESSENTRY32 pe32;
	hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnap != NULL)
	{
		if (Process32First(hSnap, &pe32))
		{
			if (strcmp(pe32.szExeFile, process_name) == 0)
				return 0;
			while (Process32Next(hSnap, &pe32))
				if (strcmp(pe32.szExeFile, process_name) == 0)
					return 0;
		}
	}
	CloseHandle(hSnap);
	return 1;
}

int close_process(char *process_name) {
	HANDLE hSnap = NULL;
	PROCESSENTRY32 pe32;
	_bstr_t b(pe32.szExeFile);
	const char *szExe = b;
	hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnap != NULL)
	{
		if (Process32First(hSnap, &pe32))
		{
			if (strcmp(szExe, process_name) == 0) {
				TerminateProcess(pe32.th32ProcessID, NO_ERROR);
				return 0;
			}
			while (Process32Next(hSnap, &pe32)) {
				if (szExe, process_name) == 0) {
					TerminateProcess(pe32.th32ProcessID, NO_ERROR);
					return 0;
				}
			}
		}
	
		return 1;
}
